from datetime import datetime
import time
import json
import requests
import numpy as np
import pandas as pd
import sqlite3
from sqlite3 import Error


class OpenDotaAPI():
    def __init__(self, verbose=False):
        self.verbose = verbose
        self.last_match_id = 0

    def _call(self, url, parameters, tries=2):
        for i in range(tries):
            try:
                if self.verbose: print("Sending API request... ", end="", flush=True)
                resp = requests.get(url, params=parameters, timeout=20)
                load_resp = json.loads(resp.text)
                if self.verbose: print("done")
                return load_resp
            except Exception as e:
                print("failed. Trying again in 5s")
                print(e)
                time.sleep(5)
        else:
            ValueError("Unable to connect to OpenDota API")

    def get_recent_matches(self, use_last_match=False):
        params = dict()
        if use_last_match:
            params['less_than_match_id'] = self.last_match_id
        url = "https://api.opendota.com/api/publicMatches"
        matches = self._call(url, params)
        self.last_match_id = min([item['match_id'] for item in matches])
        return matches


DB_CONNECTION = r".\db\pythonsqlite.db"
sql_create_matches_table = """ CREATE TABLE IF NOT EXISTS matches (
                                    match_id INTEGER PRIMARY KEY,
                                    match_seq_num INTEGER,
                                    radiant_win INTEGER,
                                    start_time INTEGER,
                                    duration INTEGER,
                                    avg_mmr INTEGER,
                                    num_mmr INTEGER,
                                    lobby_type INTEGER,
                                    game_mode INTEGER,
                                    avg_rank_tier INTEGER,
                                    num_rank_tier INTEGER,
                                    cluster INTEGER,
                                    radiant_team text,
                                    dire_team text
                                ); """


def main(sleep_time=1, number_of_requests=100):
    """ create a database connection to a SQLite database """
    conn = None
    try:
        conn = sqlite3.connect(DB_CONNECTION)
        create_table(conn, sql_create_matches_table)
        print(sqlite3.version)
        api = OpenDotaAPI(verbose=True)
        for i in range(number_of_requests):
            recent_matches = api.get_recent_matches(use_last_match=True)
            insert_matches_data(conn, recent_matches)
            time.sleep(sleep_time)
    except Error as e:
        print(e)
    finally:
        if conn:
            conn.close()


def create_table(conn, create_table_sql):
    """ create a table from the create_table_sql statement
    :param conn: Connection object
    :param create_table_sql: a CREATE TABLE statement
    :return:
    """
    try:
        c = conn.cursor()
        c.execute(create_table_sql)
    except Error as e:
        print(e)


def insert_matches_data(conn, data):
    cur = conn.cursor()
    columns = ', '.join(data[0].keys())
    placeholders = ', '.join('?' * len(data[0]))
    for match in data:
        sql = 'INSERT INTO matches ({}) VALUES ({})'.format(columns, placeholders)
        cur.execute(sql, tuple(match.values()))
    conn.commit()
    return cur.lastrowid


if __name__ == '__main__':
    main(number_of_requests=10000)
